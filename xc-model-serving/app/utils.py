# -*- coding:utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import numpy as np
import datetime
from PIL import Image
# python 3
from urllib.request import urlopen
from io import BytesIO
from urllib.error import URLError, HTTPError


def url2img(url, timeout=30):
    ''' Get image resource from URL or BASE64 and convert it to a numpy ndarray object

    :param url: either a URL string or BASE64 encoded string
    :return: a numpy ndarray object
    '''
    img = None

    try:
        t1 = datetime.datetime.now()
        response = urlopen(url, timeout=timeout)
        t2 = datetime.datetime.now()
        print('url2img: url {}, time used by urlopen {} sec'.format(url, (t2-t1).total_seconds()))
    except HTTPError as e:
        logger.warning('url2img: HTTPError; error code: {}'.format(e.code))
    except URLError as e:
        logger.warning('url2img: URLError; details: {}'.format(e.reason))
    except:
        logger.warning('url2img: unknown errors')
    else:
        t1 = datetime.datetime.now()
        fp = BytesIO(response.read())
        img = filepath2img(fp)
        fp.close()
        response.close()
        del fp, response
        t2 = datetime.datetime.now()
        print('url2img: url {}, time to load image file obj to ndarray: {} sec'.format(url, (t2 - t1).total_seconds()))

    return img

def filepath2img(fpath):
    ''' Load image from a given file path or file alike object, and convert it to a numpy ndarray object

    :param fpath: file path in file system
    :return: a numpy ndarray object
    '''

    try:
        img = Image.open(fpath)
    except IOError:
        logger.warning('filepath2img: IOError, file cannot be found; {}'.format(fpath))
        return None
    except Exception as e:
        logger.warning('filepath2img: Exception details: {}'.format(e))
        return None
    else:
        if img.mode != "RGB":
            img = img.convert("RGB")
        # note: convert obj to ndarray will also help make a copy in memory.
        # thus, the file-alike resource used in Image.open() could be released.
        return np.array(img)


def mkdir_p(path):
    ''' make a folder in file system '''
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if os.path.isdir(path):
            pass
        else:
            raise


if __name__ == '__main__':
    url = "http://pic.88meishi.com/datu01/2010120201305235625.jpg"
    img_obj = url2img(url)
    print(type(img_obj), img_obj)
