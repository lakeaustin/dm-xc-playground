# -*- coding:utf-8 -*-
import time, queue
from multiprocessing.managers import BaseManager

import numpy as np
from PIL import Image


def filepath2img(fpath):
    ''' Load image from a given file path or file alike object, and convert it to a numpy ndarray object

    :param fpath: file path in file system
    :return: a numpy ndarray object
    '''

    try:
        img = Image.open(fpath)
    except IOError:
        print('filepath2img: IOError, file cannot be found; {}'.format(fpath))
        return None
    except Exception as e:
        print('filepath2img: Exception details: {}'.format(e))
        return None
    else:
        if img.mode != "RGB":
            img = img.convert("RGB")
        # note: convert obj to ndarray will also help make a copy in memory.
        # thus, the file-alike resource used in Image.open() could be released.
        return np.array(img)


class IPCClient(BaseManager):
    def __init__(self, ip='127.0.0.1', port=12000):
        super(IPCClient, self).__init__(address=(ip, port), authkey=b'default-auth-key')

        # 由于这个QueueManager只从网络上获取Queue，所以注册时只提供名字:
        BaseManager.register('get_request_queue')
        BaseManager.register('get_response_queue')

        # 从网络连接:
        print('Connect to server {}:{}...'.format(ip, port))
        self.connect()

        # 获取Queue的对象:
        self._request_queue = self.get_request_queue()
        self._response_queue = self.get_response_queue()

    @property
    def request_queue(self):
        return self._request_queue

    @property
    def response_queue(self):
        return self._response_queue

    def put_request(self, payload):
        self._request_queue.put(payload)

    def get_response(self):

        try:
            res = self._response_queue.get()
        except EOFError as e:
            raise e

        return res

import torch
import torchvision
def torch_preprocess(img, target_size=(224, 224), mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]):
    ''' use torch backend to resize and then normalize the input image (numpy ndarray).

    example for ResNet:
        torch_preprocess(img, (224,224), [0.485, 0.456, 0.406], [0.229, 0.224, 0.225])

    :param img: numpy ndarray, in (x, y, c) shape and RGB encoded.
    :param target_size: target size this function will resize image to
    :return: a numpy ndarray, 3D, with the target_size
    '''
    assert (isinstance(img, np.ndarray) and len(img.shape) == 3)

    normalize = torchvision.transforms.Normalize(mean=mean, std=std)

    transform_pipeline = torchvision.transforms.Compose([
        torchvision.transforms.Resize(target_size),
        torchvision.transforms.ToTensor(),
        normalize,
    ])

    pil_img = Image.fromarray(img)
    output_img = transform_pipeline(pil_img)

    return output_img.numpy()

if __name__ == '__main__':
    ''' test '''
    import datetime
    from multiprocessing.pool import ThreadPool

    client = IPCClient('127.0.0.1', 5000)
    payload1 = filepath2img('images/cat.jpg')
    payload = torch_preprocess(payload1)
    print(type(payload), payload.shape)

    t1 = datetime.datetime.now()
    print('Sending request with payloads...')

    pool = ThreadPool(processes=100)

    num_req = 1000
    for i in range(num_req):
        pool.apply_async(client.put_request, (payload, ))

    t2 = datetime.datetime.now()
    print('Get responses from server...')

    idx = 0
    while True:
        idx += 1
        res = client.get_response()
        if idx % 20 == 0:
            t3 = datetime.datetime.now()
            print('t1 {} t2 {} t3 {}: processed {} requests in {} sec from sending 1st request'.format(t1, t2, t3, idx, (t3 - t2).total_seconds()))
        # print('response got from the server: length {} content {}'.format(len(res), res))

