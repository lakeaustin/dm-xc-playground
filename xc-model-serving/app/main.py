# -*- coding:utf-8 -*-

import os
import optparse
import logging
from ipc_server import IPCServer
from servables.pytorch_classifer import PytorchClassifer
import time, threading


def parse_args_from_terminal():
    """
    Parse command line options
    """
    parser = optparse.OptionParser()

    parser.add_option(
        '--logfile',
        help="log file path",
        type='str', default="/tmp/model-serving.log")

    parser.add_option("--ipc_port",
                      help="Port to listen on for gRPC API",
                      type='int', default=12000)
    parser.add_option("--grpc_port",
                      help="Port to listen on for gRPC API",
                      type='int', default=10000)
    parser.add_option("--rest_api_port",
                      help="Port to listen on for HTTP/REST API. If set to zero " \
                           "HTTP/REST API will not be exported. This port must be " \
                           "different than the one specified in --grpc_port.",
                      type='int', default=8080)

    parser.add_option("--enable_batching",
                      help="enable batching",
                      type='int', default=0)

    parser.add_option("--model_config_file",
                      help="If non-empty, read a yum server config" \
                           "file from the supplied file name, and serve the " \
                           "models in that file. This config file can be used to " \
                           "specify multiple models to serve. (If "
                           "used, --model_name, --model_base_path are ignored.)")
    parser.add_option("--model_name",
                      help="name of model (ignored if --model_config_file flag is set)",
                      type='str')
    parser.add_option("--model_base_path",
                      help="path to export (ignored if --model_config_file flag is set, otherwise required)",
                      type='str')

    parser.add_option("--file_system_poll_wait_seconds",
                      help="interval in seconds between each poll of the file system for new model version",
                      type='int', default=60)

    parser.add_option("--enable_model_warmup",
                      help="Enables model warmup to reduce first request latency.",
                      type='int', default=1)

    return parser.parse_args()


from multiprocessing.pool import ThreadPool, Pool
import datetime
# 新线程执行的代码:
def loop(server, model):
    print('thread %s is running...' % threading.current_thread().name)

    while True:
        payload = server.request_queue.get()
        print('thread: %s, get payload at {}' % threading.current_thread().name, datetime.datetime.now())
        #print('thread name %s' % threading.current_thread().name)
        # print('payload type: {}'.format(type(payload)))
        # print('payload: {}'.format(payload))
        res = model.predict(payload, enable_pp=False)
        print('thread: %s, predict finished at {}' % threading.current_thread().name, datetime.datetime.now())
        server.response_queue.put(res)
        print('thread: %s, put finished at {}' % threading.current_thread().name, datetime.datetime.now())


def main(opts, args):
    from multiprocessing.pool import ThreadPool
    _http_server = None
    _grpc_server = None
    _model_service = None
    _prediction_service = None

    # build and start IPC server
    _ipc_server = IPCServer('127.0.0.1', 5000)

    model = PytorchClassifer()
    model.load_model('test/weights/resnet18_catcheese.pth.tar')
    print('finished model loading.')


    print('thread %s is running...' % threading.current_thread().name)
    pool = ThreadPool(processes=20)
    pool_resutls = []
    for i in range(20):
        print('index: {}'.format(i))
        res = pool.apply_async(loop, args=(_ipc_server, model))
        pool_resutls.append(res)

    for r in pool_resutls:
        r.wait()
    pool.close()
    pool.join()

    # print('thread %s is running...' % threading.current_thread().name)
    # pool = Pool(processes=20)
    # pool_resutls = []
    # for i in range(20):
    #     print('index: {}'.format(i))
    #     res = pool.apply_async(loop, args=(_ipc_server, model))
    #     pool_resutls.append(res)
    #
    # for r in pool_resutls:
    #     r.wait()
    # pool.close()
    # pool.join()



    # t1 = threading.Thread(target=loop, args=(_ipc_server, model), name='prediction-service-01')
    # t1.start()
    #
    # t2 = threading.Thread(target=loop, args=(_ipc_server, model), name='prediction-service-02')
    # t2.start()

    # t1.join()
    # t2.join()
    print('thread %s ended.' % threading.current_thread().name)



if __name__ == '__main__':
    opts, args = parse_args_from_terminal()
    main(opts, args)
