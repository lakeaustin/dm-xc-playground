# -*- coding:utf-8 -*-
from __future__ import absolute_import
from __future__ import division
import os
import logging
import numpy as np
import datetime
from .api.darknet as dn
from .api.darknet import BOX, DETECTION, IMAGE, METADATA


class DarknetDetector(object):

    def __init__(self, use_gpu=True, gpu_ids='0'):

        if use_gpu and len(gpu_ids) > 1:
            # note: multi-gpu is only supported in the training step.
            #       refer: https://github.com/pjreddie/darknet/issues/777
            gpu_ids = gpu_ids[0]

        self.use_gpu = use_gpu
        if self.use_gpu:
            self.gpu_id = int(gpu_ids)
            dn.set_gpu(self.gpu_id)

        self.net = None
        self.meta = None


    def load_model(self, model_file, weight_file, meta_file=None):
        # self.net = dn.load_net("cfg/yolov3-tiny.cfg", "yolov3-tiny.weights", 0)
        # self.meta = dn.load_meta("cfg/coco.data")

        self.net = dn.load_net(model_file, weight_file, 0)
        if meta_file:
            self.meta = dn.load_meta(meta_file)

    def predict(self, image, thresh=.5, hier_thresh=.5, nms=.45):

        if isinstance(image, np.ndarray):
            # image object must be an ndarray with color channel in RGB channel
            im = dn.array_to_image(image)
        elif isinstance(image, str) or isinstance(image, bytes):
            im = dn.load_image(image, 0, 0)
        else:
            raise ValueError('unexpected type of the image parameter.')

        net = self.net
        meta = self.meta

        num = dn.c_int(0)
        pnum = dn.pointer(num)
        t1 = datetime.datetime.now()
        dn.predict_image(net, im)
        t2 = datetime.datetime.now()
        print('inference time: {} sec'.format((t2 - t1).total_seconds()))

        dets = dn.get_network_boxes(net, im.w, im.h, thresh, hier_thresh, None, 0, pnum)
        num = pnum[0]
        if (nms): dn.do_nms_obj(dets, num, meta.classes, nms);

        res = []
        for j in range(num):
            for i in range(meta.classes):
                if dets[j].prob[i] > 0:
                    b = dets[j].bbox
                    res.append((meta.names[i], dets[j].prob[i], (b.x, b.y, b.w, b.h)))

        res = sorted(res, key=lambda x: -x[1])
        dn.free_image(im)
        dn.free_detections(dets, num)
        return res


if __name__ == '__main__':
    det = DarknetDetector()
    det.load_model("yolov3-tiny.cfg", "yolov3-tiny.weights", "coco.data")

    r = det.predict("data/dog.jpg")
    # [('dog', 0.9993382096290588, (224.183837890625, 378.4219970703125, 178.6082305908203, 328.1789855957031)),
    # ('bicycle', 0.993359386920929, (344.39581298828125, 286.0813903808594, 489.4234619140625, 323.85687255859375)),
    # ('truck', 0.9153259992599487, (580.7306518554688, 125.11750030517578, 208.38381958007812, 87.00250244140625))]
    print(r)
