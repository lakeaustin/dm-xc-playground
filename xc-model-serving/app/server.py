# -*- coding:utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import optparse
import logging


class Server(object):

    def __init__(self):
        self._http_server = None
        self._grpc_server = None
        self._model_service = None
        self._prediction_service = None

    def start(self):
        pass

    def stop(self):
        pass


if __name__ == '__main__':
    pass
