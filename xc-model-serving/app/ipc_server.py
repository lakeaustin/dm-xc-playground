# -*- coding:utf-8 -*-
import time, queue
from multiprocessing.managers import BaseManager

class IPCServer(BaseManager):
    def __init__(self, ip='127.0.0.1', port=12000):

        super(IPCServer, self).__init__(address=(ip, port), authkey=b'default-auth-key')

        # Get request from client
        self._request_queue = queue.Queue()
        # 接收结果的队列:
        self._response_queue = queue.Queue()

        # 把两个Queue都注册到网络上, callable参数关联了Queue对象:
        BaseManager.register('get_request_queue', callable=lambda: self._request_queue)
        BaseManager.register('get_response_queue', callable=lambda: self._response_queue)
        self.start()
        print('server started.')

    @property
    def request_queue(self):
        return self.get_request_queue()

    @property
    def response_queue(self):
        return self.get_response_queue()

    def listen(self):
        while True:
            payload = self.request_queue.get()


if __name__ == '__main__':
    ''' unit test '''
    # build and start IPC server
    server = IPCServer('127.0.0.1', 5000)

    while True:
        try:
            payload = server.request_queue.get()
            print('payload type: {}'.format(type(payload)))
            # print('payload: {}'.format(payload))

            time.sleep(1)
            server.response_queue.put({'results': [1,2], 'results2': {'model1': [0.5, 0.2, 0.3]}})
        except queue.Empty:
            print('task queue is empty.')
        time.sleep(1)
