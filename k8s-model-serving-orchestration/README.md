## Goal

You can use yaml config files in this folder to deploy
various components (containerized model workers, web services,
metric servers, proxies, sidecars etc) in k8s, and perform
end-to-end integration test.

The organization of code tries to follow that of a Helm chart.

## Dependencies

- K8S and Kakfa are set up as in `../kubernetes-vagrant-centos-cluster`.
- Model docker images, intra-pod model manager are ready in `../xc-model-serving`. 
- Cross-pod model manager (kafka) docker image is ready in `../xc-model-pipe-manager`.
