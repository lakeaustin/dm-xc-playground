# Python Kafka Example For consuming images

Example Python App Consuming and Producing to Kafka topic

## Dependencies

- `docker` and `docker-compose`.
- `python` and `pip install kafka-python`

## Quickstart

- Set up kafka. `docker-compose up -d`
- Prepare nodejs producer. `cd nodejs; bash docker-build.sh`
- Use nodejs to read an image and produce a payload with a mini-batch of one image. `cd nodejs; bash docker-run.sh`
- Consume this payload with python. `python consume.py`. You should see:

```
model-input:0:0: key=None value={"imagesBase64":["iVBORw0KGgoAAAANSUhEUgAAANwAAADcCAIAAACUOFjWAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6Jg
0: (220, 220)
```

- Clean up. `docker rm -f node`, `docker-compose down`

The python consumer does not write the result to a kafka topic yet.

## Next steps

- Play with `nodejs/produce.js` (remember to do `docker-build.sh`) or `produce.py` to produce more messages into some topic.
- Write a python kafka consumer to do inference on image mini-batch, and write result to `model-output`.
- Write k8s yml files to deploy the consumer as a Job (with parallelism).
- Write a model-inference-metrics consumer that listens to `model-input` and `model-output`, and compute aggregated metrics (e.g. # of requests in the last minute, # or errors etc). Need a db container (maybe redis or postgres) to persist the metrics and do periodic cleanup (TTL).
- Write stress test scripts to pump images into kafka. Adjust stress test parameters (rates per second, duration, peaks etc), and observe the metrics.
- Explore different kafka producer and consumer parameters, e.g. whether to compress message.

