var fs = require('fs');
var kafka = require('kafka-node'),
    Producer = kafka.Producer,
    client = new kafka.Client(
        '0.0.0.0:2181'
    ),
    producer = new Producer(
        client, 
        {
            requireAcks: 1,
            ackTimeoutMs: 100,
            partitionerType: 2
        }
    );

var payloads_old = [{
    topic: 'my-topic',
    messages: ['msg-from-nodejs'],
    attributes: 0,
}];

var imageAsBase64 = fs.readFileSync('./1.png', 'base64');
console.log(imageAsBase64.slice(200));
var payloads = [
    {
        topic: 'model-input',
        messages: [JSON.stringify({
            'imagesBase64': [imageAsBase64],
            'model_request_id': 'request3292',
        })],
        attributes: 0,
    }
];

producer.on('ready', function() {
    producer.send(payloads, function (err, data) {
        console.log(data);
    });
});

producer.on('error', function(err) {
    console.log(err);
    process.exit(1);
});



function sendMsg(uuid,imgStr){
	producer.send(payloads,function(err,data){
		console.log("send 1 msg");
	});
}


setInterval(sendMsg, 1000);

