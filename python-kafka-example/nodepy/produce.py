from kafka import KafkaProducer
from kafka.errors import KafkaError
import threading



def kafka_producer_init():
    producer = KafkaProducer(
        bootstrap_servers=['0.0.0.0:9092'],
        retries=0,
        max_block_ms=5000,
        request_timeout_ms=2000,
    )

    # Asynchronous by default
    future = producer.send('my-topic', b'raw_bytes')

    # Block for 'synchronous' sends
    try:
        record_metadata = future.get(timeout=10)
    except KafkaError:
        # Decide what to do if produce request failed...
        raise

    print(record_metadata)

    timer = threading.Timer(1, kafka_producer)
    timer.start()


if __name__ == "__main__":
    timer = threading.Timer(1, kafka_producer)
    timer.start()
