# -*- coding: utf-8 -*-

from kafka import KafkaConsumer, KafkaProducer
from kafka.errors import KafkaError
import json
from PIL import Image
from io import BytesIO
import base64
import time
import threading
import six


"""
Message format:
[{uuid:"xxxxx",image:"xxxx"} ,……]
例：
[
    {
         uuid:"89931576-54b7-4625-9c0e-518f93d8bdb7",
         image:"/9j/4AAQSkZJRgAB……"
     }
]
"""


# To consume latest messages and auto-commit offsets
# TOPIC = 'XMC-ETALK-FRAME'
# KAFKA_SERVER = ['192.168.3.106:9092', '192.168.3.107:9092', '192.168.3.108:9092']
# KAFKA_SERVER = ['node6:9092', 'node7:9092', 'node8:9092']

TOPIC = "model-input"
KAFKA_SERVER = ['0.0.0.0:9092']



def     msg2payload(msg):
    # payload = json.loads(msg.value.decode("utf-8"))
    payload = json.loads(msg.decode("utf-8"))
    return payload

def get_kafka_earliest_msg():
    consumer = KafkaConsumer(TOPIC,
                             group_id='my-group',
                             # auto_offset_reset='earliest',
                             # auto_offset_reset='latest',
                             bootstrap_servers=['0.0.0.0:9092'])




    for message in consumer:
        # message value and key are raw bytes -- decode if necessary!
        # e.g., for unicode: `message.value.decode('utf-8')`
        print ("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                              message.offset, message.key,
                                              message.value[:100]))
        print(type(message.value))
        print(type(consumer))
        if message.topic == TOPIC:
            payload = msg2payload(message.value)
            for i, img_b64 in enumerate(payload['imagesBase64']):
                im = Image.open(BytesIO(base64.b64decode(img_b64)))
                print('{}: ({}, {})'.format(i, im.width, im.height))
                #yield i, im.width, im.height



def poll_kafka_msg():
    consumer = KafkaConsumer(bootstrap_servers=KAFKA_SERVER)
    consumer.subscribe(topics=TOPIC)

    while True:
        # if no message 'poll' will be block??
        messages = consumer.poll(timeout_ms=5) #.values()
        # print(messages)
        if messages != {}:
            print(type(messages), "type of messages")

            #TODO: how to handle timeout???

            print("number of receive messages:",len(messages))


            for partition, msgs in six.iteritems(messages):
                if partition.topic == TOPIC:

                    print('partition', partition.partition)
                    print('~~~')

                    for msg in msgs:
                        print("%s:%d:%d: key=%s value=%s" % (msg.topic, msg.partition,
                                                                    msg.offset, msg.key,
                                                                    msg.value[:100]))

                        if msg.topic == TOPIC:
                            payload = msg2payload(msg.value)
                            for p in payload:
                                print(p['uuid'])
                                img_b64 = p['image']
                                im = Image.open(BytesIO(base64.b64decode(img_b64)))
                                print(': ({}, {})'.format(im.width, im.height))
                                im.save('ttt.jpg')
                                return




if __name__ == "__main__":
    # timer = threading.Timer(1, kafka_producer)
    # timer.start()
    # kafka_producer()
    # poll_kafka_msg()
    get_kafka_earliest_msg()


