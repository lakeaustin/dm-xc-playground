# -*- coding:utf-8 -*-
# task_worker.py

import time, sys, queue
from multiprocessing.managers import BaseManager



import os
import sys
import torch
import torchvision.models as models
import torchvision.transforms as transforms
from PIL import Image
import psutil
import datetime
import pandas as pd
import numpy as np

class Predictor:
    model_names = sorted(name for name in models.__dict__
                         if name.islower() and not name.startswith("__")
                         and callable(models.__dict__[name]))

    def __init__(self, arch='resnet18', checkpoint='model_best.pth.tar'):
        device = torch.device("cuda")
        self.model = models.__dict__[arch]()

        # self.model = torch.nn.DataParallel(models.__dict__[arch](), device_ids=[0])
        #self.checkpoint = torch.load(os.path.join(os.path.dirname(__file__), checkpoint))
        self.checkpoint = torch.load(os.path.join(os.path.dirname(__file__), checkpoint), map_location="cuda:0")

        self.model.load_state_dict(self.checkpoint['state_dict'])
        self.labels = self.checkpoint['labels']
        self.model.to(device)
        self.model.eval()

        self.time_history = []

    def predict(self, input_image, use_cuda=False, verbose=True):
        if isinstance(input_image, np.ndarray):
            img_pil = Image.fromarray(input_image)
            print('debug:saving image to disk...')
            img_pil.save('img-received-by-server.jpg')
        else:
            img_pil = Image.open(input_image)

        t1 = datetime.datetime.now()

        normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        processor = transforms.Compose([transforms.Scale(256), transforms.CenterCrop(224),
                                        transforms.ToTensor(), normalize, ])
        img_tensor = processor(img_pil)
        img_tensor.unsqueeze_(0)

        t2 = datetime.datetime.now()

        img_variable = torch.autograd.Variable(img_tensor)
        if use_cuda:
            img_variable = img_variable.to(torch.device('cuda'))

        t3 = datetime.datetime.now()
        # (pp_time, inference_time)
        self.time_history.append(((t2 - t1).total_seconds(), (t3 - t2).total_seconds()))

        output = self.model(img_variable)
        result_index = output.data.cpu().numpy().argmax()
        result = self.labels[result_index]
        print("Input file path: {}; Prediction Result {}".format(input_image, result))
        return result


# 发送任务的队列:
task_queue = queue.Queue()
# 接收结果的队列:
result_queue = queue.Queue()

# 创建类似的QueueManager:
class QueueManager(BaseManager):
    pass


if __name__ == '__main__':

    model = Predictor()
    print('finished model loading.')
    #
    # # 由于这个QueueManager只从网络上获取Queue，所以注册时只提供名字:
    # QueueManager.register('get_task_queue')
    # QueueManager.register('get_result_queue')
    #
    # # 连接到服务器，也就是运行task_master.py的机器:
    # server_addr = '127.0.0.1'
    # print('Connect to server %s...' % server_addr)
    # # 端口和验证码注意保持与task_master.py设置的完全一致:
    # m = QueueManager(address=(server_addr, 5000), authkey=b'abc')
    # # 从网络连接:
    # m.connect()
    # # 获取Queue的对象:
    # task = m.get_task_queue()
    # result = m.get_result_queue()
    #
    # # 从task队列取任务,并把结果写入result队列:
    # while True:
    #     try:
    #         payload = task.get(timeout=3)
    #         print('payload: {}'.format(payload))
    #         r = model.predict(payload)
    #         time.sleep(1)
    #         result.put(r)
    #     except queue.Queue.Empty:
    #         print('task queue is empty.')
    #     time.sleep(1)


    # 把两个Queue都注册到网络上, callable参数关联了Queue对象:
    QueueManager.register('get_task_queue', callable=lambda: task_queue)
    QueueManager.register('get_result_queue', callable=lambda: result_queue)

    # 绑定端口5000, 设置验证码'abc':
    manager = QueueManager(address=('', 5000), authkey=b'abc')

    # 启动Queue:
    manager.start()
    # 获得通过网络访问的Queue对象:
    task = manager.get_task_queue()
    result = manager.get_result_queue()

    print('server started.')

    # 从task队列取任务,并把结果写入result队列:
    while True:
        try:
            payload = task.get(timeout=3)
            print('payload type: {}'.format(type(payload)))
            #print('payload: {}'.format(payload))
            r = model.predict(payload, use_cuda=True)
            time.sleep(1)
            result.put(r)
        except queue.Empty:
            print('task queue is empty.')
        time.sleep(1)
