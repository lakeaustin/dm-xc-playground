# -*- coding:utf-8 -*-
# task_master.py

import random, time, queue
from multiprocessing.managers import BaseManager
import numpy as np
from PIL import Image



# 从BaseManager继承的QueueManager:
class QueueManager(BaseManager):
    pass


def filepath2img(fpath):
    ''' Load image from a given file path or file alike object, and convert it to a numpy ndarray object

    :param fpath: file path in file system
    :return: a numpy ndarray object
    '''

    try:
        img = Image.open(fpath)
    except IOError:
        print('filepath2img: IOError, file cannot be found; {}'.format(fpath))
        return None
    except Exception as e:
        print('filepath2img: Exception details: {}'.format(e))
        return None
    else:
        if img.mode != "RGB":
            img = img.convert("RGB")
        # note: convert obj to ndarray will also help make a copy in memory.
        # thus, the file-alike resource used in Image.open() could be released.
        return np.array(img)


if __name__ == '__main__':
    #
    # # 把两个Queue都注册到网络上, callable参数关联了Queue对象:
    # QueueManager.register('get_task_queue', callable=lambda: task_queue)
    # QueueManager.register('get_result_queue', callable=lambda: result_queue)
    #
    # # 绑定端口5000, 设置验证码'abc':
    # manager = QueueManager(address=('', 5000), authkey=b'abc')
    #
    # # 启动Queue:
    # manager.start()
    # # 获得通过网络访问的Queue对象:
    # task = manager.get_task_queue()
    # result = manager.get_result_queue()


    # 由于这个QueueManager只从网络上获取Queue，所以注册时只提供名字:
    QueueManager.register('get_task_queue')
    QueueManager.register('get_result_queue')

    # 连接到服务器，也就是运行task_master.py的机器:
    server_addr = '127.0.0.1'
    print('Connect to server %s...' % server_addr)
    # 端口和验证码注意保持与task_master.py设置的完全一致:
    m = QueueManager(address=(server_addr, 5000), authkey=b'abc')
    # 从网络连接:
    m.connect()
    # 获取Queue的对象:
    task = m.get_task_queue()
    result = m.get_result_queue()

    # 放几个任务进去:
    for i in range(10):
        payload = filepath2img('images/cat.jpg')
        task.put(payload)
        time.sleep(3)

    time.sleep(5)

    # 从result队列读取结果:
    print('Try get results...')
    for i in range(10):
        r = result.get(timeout=10)
        print('Result: %s' % r)

    print('exit')
