# README

- 目标: 尝试 python queue 做进程之间的通信，解耦合 predict server vs. client

@zz (zhangzhang)  关于 Pipeline Manager 和  Model Serving之间通信
（无论是由一个主进程起， 还是两个独立的主进程），你说 python queue or redis queue,
 是不是  https://docs.python.org/2/library/multiprocessing.html  
 里的 BaseManager类和 Queue是你说的。我说的类似这个里的例子代码 
 
### Orginal example:
 
 - task_master.py
 - task_worker.py
 - src: https://www.liaoxuefeng.com/wiki/001374738125095c955c1e6d8bb493182103fac9270762a000/001386832973658c780d8bfa4c6406f83b2b3097aed5df6000
 
### Modified example for model serving

- model_server.py
- model_client.py
- model and weight file: copy from `src/master/xc-serving-playground/playground/pytorch-resnet18/model_best.pth.tar` 

run examples:
- ```python model_server.py``` and then ```python model_client.py``` 
