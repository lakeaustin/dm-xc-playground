# README


### darknet



#### setup

1. clone
1. if using GPU, make sure `gpu=1` in `Makefile`
1. run `make`
1. download config and weight files
```
wget https://pjreddie.com/media/files/yolov3-tiny.weights
wget https://github.com/pjreddie/darknet/blob/master/cfg/yolov3-tiny.cfg    

https://pjreddie.com/media/files/yolov3-tiny.weights
```
1. run `python darknet.py`

#### Use GPU in darknet.py

```
set_gpu = lib.cuda_set_device
set_gpu.argtypes = [c_int]
set_gpu(0)
```


#### test result

- yolov3-tiny


```
Loading weights from yolov3-tiny.weights...Done!
inference time: 2.593454 sec
inference time: 0.008164 sec
inference time: 0.008105 sec
inference time: 0.007904 sec
inference time: 0.007864 sec
inference time: 0.007981 sec
inference time: 0.008105 sec
inference time: 0.008435 sec
inference time: 0.008051 sec
inference time: 0.007825 sec
inference time: 0.007864 sec
inference time: 0.007766 sec
inference time: 0.007801 sec
inference time: 0.007884 sec
inference time: 0.00806 sec
inference time: 0.007816 sec
inference time: 0.007853 sec
inference time: 0.008132 sec
inference time: 0.007762 sec
inference time: 0.007751 sec
inference time: 0.007907 sec
```

- yolov3-320

```
TBA
```


### pytorch


# pytorch CAN load multi models into single GPU and predict in parallel
```commandline
python multi_predictor.py --mode smoking

```

# push the predict task to redis queue, but not work yet.
```commandline
python multi_predictor.py
```

run cmd to monitor redis task status
(move function to listen.py if you want to get more detail or Traceback)
```commandline

python listen.py
```
run

Maybe some issues on cuda!

error log:
```commandline
10:43:55 Moving job to 'failed' queue
10:43:55 default: listen.predict_work(True, 'images/cat.jpg') (9a3e1869-3228-4afd-abc6-11b4c2a6c95c)
THCudaCheck FAIL file=torch/csrc/cuda/Module.cpp line=51 error=3 : initialization error
10:43:55 RuntimeError: cuda runtime error (3) : initialization error at torch/csrc/cuda/Module.cpp:51
Traceback (most recent call last):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/worker.py", line 793, in perform_job
    rv = job.perform()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 599, in perform
    self._result = self._execute()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 605, in _execute
    return self.func(*self.args, **self.kwargs)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 213, in func
    return import_attribute(self.func_name)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/utils.py", line 152, in import_attribute
    module = importlib.import_module(module_name)
  File "/home/limingliang/pyenv/py36/lib64/python3.6/importlib/__init__.py", line 126, in import_module
    return _bootstrap._gcd_import(name[level:], package, level)
  File "<frozen importlib._bootstrap>", line 994, in _gcd_import
  File "<frozen importlib._bootstrap>", line 971, in _find_and_load
  File "<frozen importlib._bootstrap>", line 955, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 665, in _load_unlocked
  File "<frozen importlib._bootstrap_external>", line 678, in exec_module
  File "<frozen importlib._bootstrap>", line 219, in _call_with_frames_removed
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 69, in <module>
    model = Predictor()
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 32, in __init__
    self.checkpoint = torch.load(os.path.join(os.path.dirname(__file__), checkpoint), map_location=DEVICE)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 358, in load
    return _load(f, map_location, pickle_module)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 542, in _load
    result = unpickler.load()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 508, in persistent_load
    data_type(size), location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 375, in restore_location
    return default_restore_location(storage, map_location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 104, in default_restore_location
    result = fn(storage, location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 86, in _cuda_deserialize
    return obj.cuda(device)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/_utils.py", line 68, in _cuda
    with torch.cuda.device(device):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/cuda/__init__.py", line 225, in __enter__
    self.prev_idx = torch._C._cuda_getDevice()
RuntimeError: cuda runtime error (3) : initialization error at torch/csrc/cuda/Module.cpp:51
Traceback (most recent call last):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/worker.py", line 793, in perform_job
    rv = job.perform()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 599, in perform
    self._result = self._execute()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 605, in _execute
    return self.func(*self.args, **self.kwargs)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 213, in func
    return import_attribute(self.func_name)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/utils.py", line 152, in import_attribute
    module = importlib.import_module(module_name)
  File "/home/limingliang/pyenv/py36/lib64/python3.6/importlib/__init__.py", line 126, in import_module
    return _bootstrap._gcd_import(name[level:], package, level)
  File "<frozen importlib._bootstrap>", line 994, in _gcd_import
  File "<frozen importlib._bootstrap>", line 971, in _find_and_load
  File "<frozen importlib._bootstrap>", line 955, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 665, in _load_unlocked
  File "<frozen importlib._bootstrap_external>", line 678, in exec_module
  File "<frozen importlib._bootstrap>", line 219, in _call_with_frames_removed
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 69, in <module>
    model = Predictor()
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 32, in __init__
    self.checkpoint = torch.load(os.path.join(os.path.dirname(__file__), checkpoint), map_location=DEVICE)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 358, in load
    return _load(f, map_location, pickle_module)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 542, in _load
    result = unpickler.load()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 508, in persistent_load
    data_type(size), location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 375, in restore_location
    return default_restore_location(storage, map_location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 104, in default_restore_location
    result = fn(storage, location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 86, in _cuda_deserialize
    return obj.cuda(device)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/_utils.py", line 68, in _cuda
    with torch.cuda.device(device):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/cuda/__init__.py", line 225, in __enter__
    self.prev_idx = torch._C._cuda_getDevice()
RuntimeError: cuda runtime error (3) : initialization error at torch/csrc/cuda/Module.cpp:51
10:43:55 Moving job to 'failed' queue
10:43:55 default: listen.predict_work(True, 'images/cat.jpg') (33a51e31-1343-4485-b3d9-e823097eda2b)
THCudaCheck FAIL file=torch/csrc/cuda/Module.cpp line=51 error=3 : initialization error
10:43:56 RuntimeError: cuda runtime error (3) : initialization error at torch/csrc/cuda/Module.cpp:51
Traceback (most recent call last):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/worker.py", line 793, in perform_job
    rv = job.perform()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 599, in perform
    self._result = self._execute()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 605, in _execute
    return self.func(*self.args, **self.kwargs)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 213, in func
    return import_attribute(self.func_name)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/utils.py", line 152, in import_attribute
    module = importlib.import_module(module_name)
  File "/home/limingliang/pyenv/py36/lib64/python3.6/importlib/__init__.py", line 126, in import_module
    return _bootstrap._gcd_import(name[level:], package, level)
  File "<frozen importlib._bootstrap>", line 994, in _gcd_import
  File "<frozen importlib._bootstrap>", line 971, in _find_and_load
  File "<frozen importlib._bootstrap>", line 955, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 665, in _load_unlocked
  File "<frozen importlib._bootstrap_external>", line 678, in exec_module
  File "<frozen importlib._bootstrap>", line 219, in _call_with_frames_removed
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 69, in <module>
    model = Predictor()
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 32, in __init__
    self.checkpoint = torch.load(os.path.join(os.path.dirname(__file__), checkpoint), map_location=DEVICE)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 358, in load
    return _load(f, map_location, pickle_module)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 542, in _load
    result = unpickler.load()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 508, in persistent_load
    data_type(size), location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 375, in restore_location
    return default_restore_location(storage, map_location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 104, in default_restore_location
    result = fn(storage, location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 86, in _cuda_deserialize
    return obj.cuda(device)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/_utils.py", line 68, in _cuda
    with torch.cuda.device(device):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/cuda/__init__.py", line 225, in __enter__
    self.prev_idx = torch._C._cuda_getDevice()
RuntimeError: cuda runtime error (3) : initialization error at torch/csrc/cuda/Module.cpp:51
Traceback (most recent call last):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/worker.py", line 793, in perform_job
    rv = job.perform()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 599, in perform
    self._result = self._execute()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 605, in _execute
    return self.func(*self.args, **self.kwargs)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 213, in func
    return import_attribute(self.func_name)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/utils.py", line 152, in import_attribute
    module = importlib.import_module(module_name)
  File "/home/limingliang/pyenv/py36/lib64/python3.6/importlib/__init__.py", line 126, in import_module
    return _bootstrap._gcd_import(name[level:], package, level)
  File "<frozen importlib._bootstrap>", line 994, in _gcd_import
  File "<frozen importlib._bootstrap>", line 971, in _find_and_load
  File "<frozen importlib._bootstrap>", line 955, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 665, in _load_unlocked
  File "<frozen importlib._bootstrap_external>", line 678, in exec_module
  File "<frozen importlib._bootstrap>", line 219, in _call_with_frames_removed
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 69, in <module>
    model = Predictor()
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 32, in __init__
    self.checkpoint = torch.load(os.path.join(os.path.dirname(__file__), checkpoint), map_location=DEVICE)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 358, in load
    return _load(f, map_location, pickle_module)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 542, in _load
    result = unpickler.load()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 508, in persistent_load
    data_type(size), location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 375, in restore_location
    return default_restore_location(storage, map_location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 104, in default_restore_location
    result = fn(storage, location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 86, in _cuda_deserialize
    return obj.cuda(device)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/_utils.py", line 68, in _cuda
    with torch.cuda.device(device):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/cuda/__init__.py", line 225, in __enter__
    self.prev_idx = torch._C._cuda_getDevice()
RuntimeError: cuda runtime error (3) : initialization error at torch/csrc/cuda/Module.cpp:51
10:43:56 Moving job to 'failed' queue
10:43:56 default: listen.predict_work(True, 'images/cat.jpg') (8cc6b80e-41ab-4097-a204-eef60aa8ebe6)
THCudaCheck FAIL file=torch/csrc/cuda/Module.cpp line=51 error=3 : initialization error
10:43:56 RuntimeError: cuda runtime error (3) : initialization error at torch/csrc/cuda/Module.cpp:51
Traceback (most recent call last):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/worker.py", line 793, in perform_job
    rv = job.perform()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 599, in perform
    self._result = self._execute()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 605, in _execute
    return self.func(*self.args, **self.kwargs)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 213, in func
    return import_attribute(self.func_name)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/utils.py", line 152, in import_attribute
    module = importlib.import_module(module_name)
  File "/home/limingliang/pyenv/py36/lib64/python3.6/importlib/__init__.py", line 126, in import_module
    return _bootstrap._gcd_import(name[level:], package, level)
  File "<frozen importlib._bootstrap>", line 994, in _gcd_import
  File "<frozen importlib._bootstrap>", line 971, in _find_and_load
  File "<frozen importlib._bootstrap>", line 955, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 665, in _load_unlocked
  File "<frozen importlib._bootstrap_external>", line 678, in exec_module
  File "<frozen importlib._bootstrap>", line 219, in _call_with_frames_removed
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 69, in <module>
    model = Predictor()
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 32, in __init__
    self.checkpoint = torch.load(os.path.join(os.path.dirname(__file__), checkpoint), map_location=DEVICE)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 358, in load
    return _load(f, map_location, pickle_module)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 542, in _load
    result = unpickler.load()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 508, in persistent_load
    data_type(size), location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 375, in restore_location
    return default_restore_location(storage, map_location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 104, in default_restore_location
    result = fn(storage, location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 86, in _cuda_deserialize
    return obj.cuda(device)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/_utils.py", line 68, in _cuda
    with torch.cuda.device(device):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/cuda/__init__.py", line 225, in __enter__
    self.prev_idx = torch._C._cuda_getDevice()
RuntimeError: cuda runtime error (3) : initialization error at torch/csrc/cuda/Module.cpp:51
Traceback (most recent call last):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/worker.py", line 793, in perform_job
    rv = job.perform()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 599, in perform
    self._result = self._execute()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 605, in _execute
    return self.func(*self.args, **self.kwargs)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 213, in func
    return import_attribute(self.func_name)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/utils.py", line 152, in import_attribute
    module = importlib.import_module(module_name)
  File "/home/limingliang/pyenv/py36/lib64/python3.6/importlib/__init__.py", line 126, in import_module
    return _bootstrap._gcd_import(name[level:], package, level)
  File "<frozen importlib._bootstrap>", line 994, in _gcd_import
  File "<frozen importlib._bootstrap>", line 971, in _find_and_load
  File "<frozen importlib._bootstrap>", line 955, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 665, in _load_unlocked
  File "<frozen importlib._bootstrap_external>", line 678, in exec_module
  File "<frozen importlib._bootstrap>", line 219, in _call_with_frames_removed
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 69, in <module>
    model = Predictor()
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 32, in __init__
    self.checkpoint = torch.load(os.path.join(os.path.dirname(__file__), checkpoint), map_location=DEVICE)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 358, in load
    return _load(f, map_location, pickle_module)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 542, in _load
    result = unpickler.load()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 508, in persistent_load
    data_type(size), location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 375, in restore_location
    return default_restore_location(storage, map_location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 104, in default_restore_location
    result = fn(storage, location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 86, in _cuda_deserialize
    return obj.cuda(device)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/_utils.py", line 68, in _cuda
    with torch.cuda.device(device):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/cuda/__init__.py", line 225, in __enter__
    self.prev_idx = torch._C._cuda_getDevice()
RuntimeError: cuda runtime error (3) : initialization error at torch/csrc/cuda/Module.cpp:51
10:43:56 Moving job to 'failed' queue
10:43:56 default: listen.predict_work(True, 'images/cat.jpg') (d1ca20d1-c313-404d-9558-da2415a836e9)
THCudaCheck FAIL file=torch/csrc/cuda/Module.cpp line=51 error=3 : initialization error
10:43:56 RuntimeError: cuda runtime error (3) : initialization error at torch/csrc/cuda/Module.cpp:51
Traceback (most recent call last):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/worker.py", line 793, in perform_job
    rv = job.perform()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 599, in perform
    self._result = self._execute()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 605, in _execute
    return self.func(*self.args, **self.kwargs)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 213, in func
    return import_attribute(self.func_name)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/utils.py", line 152, in import_attribute
    module = importlib.import_module(module_name)
  File "/home/limingliang/pyenv/py36/lib64/python3.6/importlib/__init__.py", line 126, in import_module
    return _bootstrap._gcd_import(name[level:], package, level)
  File "<frozen importlib._bootstrap>", line 994, in _gcd_import
  File "<frozen importlib._bootstrap>", line 971, in _find_and_load
  File "<frozen importlib._bootstrap>", line 955, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 665, in _load_unlocked
  File "<frozen importlib._bootstrap_external>", line 678, in exec_module
  File "<frozen importlib._bootstrap>", line 219, in _call_with_frames_removed
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 69, in <module>
    model = Predictor()
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 32, in __init__
    self.checkpoint = torch.load(os.path.join(os.path.dirname(__file__), checkpoint), map_location=DEVICE)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 358, in load
    return _load(f, map_location, pickle_module)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 542, in _load
    result = unpickler.load()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 508, in persistent_load
    data_type(size), location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 375, in restore_location
    return default_restore_location(storage, map_location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 104, in default_restore_location
    result = fn(storage, location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 86, in _cuda_deserialize
    return obj.cuda(device)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/_utils.py", line 68, in _cuda
    with torch.cuda.device(device):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/cuda/__init__.py", line 225, in __enter__
    self.prev_idx = torch._C._cuda_getDevice()
RuntimeError: cuda runtime error (3) : initialization error at torch/csrc/cuda/Module.cpp:51
Traceback (most recent call last):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/worker.py", line 793, in perform_job
    rv = job.perform()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 599, in perform
    self._result = self._execute()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 605, in _execute
    return self.func(*self.args, **self.kwargs)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 213, in func
    return import_attribute(self.func_name)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/utils.py", line 152, in import_attribute
    module = importlib.import_module(module_name)
  File "/home/limingliang/pyenv/py36/lib64/python3.6/importlib/__init__.py", line 126, in import_module
    return _bootstrap._gcd_import(name[level:], package, level)
  File "<frozen importlib._bootstrap>", line 994, in _gcd_import
  File "<frozen importlib._bootstrap>", line 971, in _find_and_load
  File "<frozen importlib._bootstrap>", line 955, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 665, in _load_unlocked
  File "<frozen importlib._bootstrap_external>", line 678, in exec_module
  File "<frozen importlib._bootstrap>", line 219, in _call_with_frames_removed
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 69, in <module>
    model = Predictor()
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 32, in __init__
    self.checkpoint = torch.load(os.path.join(os.path.dirname(__file__), checkpoint), map_location=DEVICE)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 358, in load
    return _load(f, map_location, pickle_module)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 542, in _load
    result = unpickler.load()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 508, in persistent_load
    data_type(size), location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 375, in restore_location
    return default_restore_location(storage, map_location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 104, in default_restore_location
    result = fn(storage, location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 86, in _cuda_deserialize
    return obj.cuda(device)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/_utils.py", line 68, in _cuda
    with torch.cuda.device(device):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/cuda/__init__.py", line 225, in __enter__
    self.prev_idx = torch._C._cuda_getDevice()
RuntimeError: cuda runtime error (3) : initialization error at torch/csrc/cuda/Module.cpp:51
10:43:56 Moving job to 'failed' queue
10:43:56 default: listen.predict_work(True, 'images/cat.jpg') (6dfebd27-f344-4fe7-bd0b-936e67999432)
THCudaCheck FAIL file=torch/csrc/cuda/Module.cpp line=51 error=3 : initialization error
10:43:57 RuntimeError: cuda runtime error (3) : initialization error at torch/csrc/cuda/Module.cpp:51
Traceback (most recent call last):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/worker.py", line 793, in perform_job
    rv = job.perform()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 599, in perform
    self._result = self._execute()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 605, in _execute
    return self.func(*self.args, **self.kwargs)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 213, in func
    return import_attribute(self.func_name)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/utils.py", line 152, in import_attribute
    module = importlib.import_module(module_name)
  File "/home/limingliang/pyenv/py36/lib64/python3.6/importlib/__init__.py", line 126, in import_module
    return _bootstrap._gcd_import(name[level:], package, level)
  File "<frozen importlib._bootstrap>", line 994, in _gcd_import
  File "<frozen importlib._bootstrap>", line 971, in _find_and_load
  File "<frozen importlib._bootstrap>", line 955, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 665, in _load_unlocked
  File "<frozen importlib._bootstrap_external>", line 678, in exec_module
  File "<frozen importlib._bootstrap>", line 219, in _call_with_frames_removed
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 69, in <module>
    model = Predictor()
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 32, in __init__
    self.checkpoint = torch.load(os.path.join(os.path.dirname(__file__), checkpoint), map_location=DEVICE)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 358, in load
    return _load(f, map_location, pickle_module)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 542, in _load
    result = unpickler.load()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 508, in persistent_load
    data_type(size), location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 375, in restore_location
    return default_restore_location(storage, map_location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 104, in default_restore_location
    result = fn(storage, location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 86, in _cuda_deserialize
    return obj.cuda(device)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/_utils.py", line 68, in _cuda
    with torch.cuda.device(device):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/cuda/__init__.py", line 225, in __enter__
    self.prev_idx = torch._C._cuda_getDevice()
RuntimeError: cuda runtime error (3) : initialization error at torch/csrc/cuda/Module.cpp:51
Traceback (most recent call last):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/worker.py", line 793, in perform_job
    rv = job.perform()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 599, in perform
    self._result = self._execute()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 605, in _execute
    return self.func(*self.args, **self.kwargs)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/job.py", line 213, in func
    return import_attribute(self.func_name)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/rq/utils.py", line 152, in import_attribute
    module = importlib.import_module(module_name)
  File "/home/limingliang/pyenv/py36/lib64/python3.6/importlib/__init__.py", line 126, in import_module
    return _bootstrap._gcd_import(name[level:], package, level)
  File "<frozen importlib._bootstrap>", line 994, in _gcd_import
  File "<frozen importlib._bootstrap>", line 971, in _find_and_load
  File "<frozen importlib._bootstrap>", line 955, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 665, in _load_unlocked
  File "<frozen importlib._bootstrap_external>", line 678, in exec_module
  File "<frozen importlib._bootstrap>", line 219, in _call_with_frames_removed
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 69, in <module>
    model = Predictor()
  File "/home/limingliang/proj/pytorch_inferende/listen.py", line 32, in __init__
    self.checkpoint = torch.load(os.path.join(os.path.dirname(__file__), checkpoint), map_location=DEVICE)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 358, in load
    return _load(f, map_location, pickle_module)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 542, in _load
    result = unpickler.load()
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 508, in persistent_load
    data_type(size), location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 375, in restore_location
    return default_restore_location(storage, map_location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 104, in default_restore_location
    result = fn(storage, location)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/serialization.py", line 86, in _cuda_deserialize
    return obj.cuda(device)
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/_utils.py", line 68, in _cuda
    with torch.cuda.device(device):
  File "/home/limingliang/pyenv/py36/lib/python3.6/site-packages/torch/cuda/__init__.py", line 225, in __enter__
    self.prev_idx = torch._C._cuda_getDevice()
RuntimeError: cuda runtime error (3) : initialization error at torch/csrc/cuda/Module.cpp:51
10:43:57 Moving job to 'failed' queue
10:43:57 default: listen.predict_work(True, 'images/cat.jpg') (4c651d18-a88b-4f72-b4c8-f72458b1ebbb)
THCudaCheck FAIL file=torch/csrc/cuda/Module.cpp line=51 error=3 : initialization error
10:43:57 RuntimeError: cuda runtime error (3) : initialization error at torch/csrc/cuda/Module.cpp:51
```









