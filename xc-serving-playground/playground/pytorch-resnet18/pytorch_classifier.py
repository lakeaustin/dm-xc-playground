# -*- coding:utf-8 -*-
from __future__ import absolute_import
from __future__ import division
import os
import logging
import numpy as np
from PIL import Image
import torch
import torchvision.models as models
import torchvision.transforms as transforms
import datetime

class PytorchClassifer(object):
    model_names = sorted(name for name in models.__dict__
                         if name.islower() and not name.startswith("__")
                         and callable(models.__dict__[name]))

    def __init__(self, use_gpu=True, gpu_ids='0', arch='resnet18'):
        self.device_id = 'cuda:' + gpu_ids
        self.device = torch.device(self.device_id)
        self.use_gpu = use_gpu
        self.arch = arch
        self.model = None

    def load_model(self, model_file):

        self.model = models.__dict__[self.arch]()
        if self.use_gpu:
            self.checkpoint = torch.load(model_file, map_location=self.device)
        else:
            self.checkpoint = torch.load(model_file)

        self.model.load_state_dict(self.checkpoint['state_dict'])

        if self.use_gpu:
            self.model.to(self.device)
        self.model.eval()


    def predict(self, input_image, enable_pp=True):

        if enable_pp:
            if isinstance(input_image, np.ndarray):
                img_pil = Image.fromarray(input_image)
                # print('debug:saving image to disk...')
                # img_pil.save('img-received-by-server.jpg')
            elif isinstance(input_image, Image.Image):
                img_pil = input_image
            else:
                img_pil = Image.open(input_image)

            normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
            processor = transforms.Compose([transforms.Scale(256), transforms.CenterCrop(224),
                                            transforms.ToTensor(), normalize, ])
            img_tensor = processor(img_pil)
        else:
            # processor = transforms.Compose([transforms.ToTensor(), ])
            # img_tensor = processor(img_pil)
            assert(isinstance(input_image, np.ndarray))
            img_tensor = torch.from_numpy(input_image)

        img_tensor.unsqueeze_(0)
        img_variable = torch.autograd.Variable(img_tensor)

        if self.use_gpu:
            img_variable = img_variable.to(self.device)

        output = self.model(img_variable)

        # TODO: output.data.cpu().numpy() -> (1, 1000) vector, may need to flatten, sort, and return top N
        raw_result = output.data.cpu().numpy()
        #print(type(raw_result), raw_result.shape)
        #print('timestamp after predict: {}'.format(datetime.datetime.now()))

        return list(raw_result[0])


if __name__ == '__main__':
    ''' Test '''
    classier = PytorchClassifer(use_gpu=False)
    model_file_path = 'model_best.pth.tar'
    classier.load_model(model_file_path)

    img = np.zeros([224, 224, 3], dtype=np.uint8)
    img.fill(255)
    pil_img = Image.fromarray(img)

    img1 = np.zeros([3, 224, 224], dtype=np.uint8)
    img2 = np.zeros([224, 224, 3], dtype=np.uint8)
    r = classier.predict(pil_img, True)
    print(r)
    # r = classier.predict(img1, False)
    # print(r)
    r = classier.predict(img2, True)
    print(r)

    print('-------------------------------')
    #
    # classier2 = PytorchClassifer()
    # classier2.load_model('../../test/res/resnet_blur_model.pth')
    #
    # img1 = np.zeros([3, 224, 224], dtype=np.uint8)
    # r = classier2.predict(img1, False)
    # print(r)
    #
    # img2 = np.zeros([224, 224, 3], dtype=np.uint8)
    # r = classier2.predict(img2, True)
    # print(r)
