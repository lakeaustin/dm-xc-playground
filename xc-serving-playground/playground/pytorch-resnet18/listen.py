import redis
from rq import Worker, Queue, Connection

import os
import torch
import torchvision.models as models
import torchvision.transforms as transforms
from PIL import Image
import datetime

listen = ['default']
redis_url = "redis://localhost:6379"  # redis server 默认地址
conn = redis.from_url(redis_url)

def square_function(x):
    return x*x

# uncomment if want to track the error logs

#
# class Predictor:
#     model_names = sorted(name for name in models.__dict__
#                          if name.islower() and not name.startswith("__")
#                          and callable(models.__dict__[name]))
#
#     def __init__(self, arch='resnet18', checkpoint='model_best.pth.tar'):
#         DEVICE = 'cuda:3'
#         self.device = torch.device(DEVICE)
#         self.model = models.__dict__[arch]()
#
#         # self.model = torch.nn.DataParallel(models.__dict__[arch](), device_ids=[0])
#         # self.checkpoint = torch.load(os.path.join(os.path.dirname(__file__), checkpoint))
#         self.checkpoint = torch.load(os.path.join(os.path.dirname(__file__), checkpoint), map_location=DEVICE)
#
#         self.model.load_state_dict(self.checkpoint['state_dict'])
#         self.labels = self.checkpoint['labels']
#         self.model.to(self.device)
#         self.model.eval()
#
#         self.time_history = []
#
#     def predict(self, input_image, use_cuda=False, verbose=True):
#         img_pil = Image.open(input_image)
#
#         t1 = datetime.datetime.now()
#
#         normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
#         processor = transforms.Compose([transforms.Scale(256), transforms.CenterCrop(224),
#                                         transforms.ToTensor(), normalize, ])
#         img_tensor = processor(img_pil)
#         img_tensor.unsqueeze_(0)
#
#         t2 = datetime.datetime.now()
#
#         img_variable = torch.autograd.Variable(img_tensor)
#         if use_cuda:
#             img_variable = img_variable.to(self.device)
#
#         t3 = datetime.datetime.now()
#         # (pp_time, inference_time)
#         self.time_history.append(((t2 - t1).total_seconds(), (t3 - t2).total_seconds()))
#
#         output = self.model(img_variable)
#         result_index = output.data.cpu().numpy().argmax()
#
#         result = self.labels[result_index]
#         # print("Input file path: {}; Prediction Result {}".format(input_image, result))
#         return result
#
# model = Predictor()
#
# #from multi_predictor import Predictor
# def predict_work(use_cuda, filePath):
#
#      print("Start to predict!")
#      result =  model.predict(filePath, use_cuda=use_cuda, verbose=False)
#      print("End fo predict!")
#      return result


if __name__ == '__main__':
    with Connection(conn):  # 建立与redis server的连接
        worker = Worker(list(map(Queue, listen)))  # 建立worker监听给定的队列
        worker.work()


