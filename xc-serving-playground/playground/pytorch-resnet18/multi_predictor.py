import argparse
import os
import sys
import torch
import torchvision.models as models
import torchvision.transforms as transforms
from PIL import Image

import psutil
import datetime
import pandas as pd
from time import sleep

import multiprocessing

import os

import redis
from rq import Worker, Queue, Connection
from rq.job import Job


# from listen import predict_work

os.system("export CUDA_VISIBLE_DEVICES=3")


from redis import Redis

redis_conn = Redis()
q = Queue(connection=redis_conn)


class Predictor:
    model_names = sorted(name for name in models.__dict__
                         if name.islower() and not name.startswith("__")
                         and callable(models.__dict__[name]))

    def __init__(self, arch='resnet18', checkpoint='model_best.pth.tar'):
        DEVICE = 'cuda:3'
        self.device = torch.device(DEVICE)
        self.model = models.__dict__[arch]()

        # self.model = torch.nn.DataParallel(models.__dict__[arch](), device_ids=[0])
        # self.checkpoint = torch.load(os.path.join(os.path.dirname(__file__), checkpoint))
        self.checkpoint = torch.load(os.path.join(os.path.dirname(__file__), checkpoint), map_location=DEVICE)

        self.model.load_state_dict(self.checkpoint['state_dict'])
        self.labels = self.checkpoint['labels']
        self.model.to(self.device)
        self.model.eval()

        self.time_history = []

    def predict(self, input_image, use_cuda=False, verbose=True):
        img_pil = Image.open(input_image)

        t1 = datetime.datetime.now()

        normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        processor = transforms.Compose([transforms.Scale(256), transforms.CenterCrop(224),
                                        transforms.ToTensor(), normalize, ])
        img_tensor = processor(img_pil)
        img_tensor.unsqueeze_(0)

        t2 = datetime.datetime.now()

        img_variable = torch.autograd.Variable(img_tensor)
        if use_cuda:
            img_variable = img_variable.to(self.device)

        t3 = datetime.datetime.now()
        # (pp_time, inference_time)
        self.time_history.append(((t2 - t1).total_seconds(), (t3 - t2).total_seconds()))

        output = self.model(img_variable)
        result_index = output.data.cpu().numpy().argmax()

        result = self.labels[result_index]
        # print("Input file path: {}; Prediction Result {}".format(input_image, result))
        return result


def smoking_test(i=1, use_cuda=True, max_num_loops=10000):
    ''' measure 1) host CPU memory util % 2) preprocess and inference time

    :param max_num_loops:
    :return:
    '''

    print("---------------")
    print("--This is process: {}   --".format(i))
    print("---------------")

    mem_history = []
    image_files = ['images/cat.jpg', 'images/cheese.jpg']

    model = Predictor()
    mem_info = psutil.virtual_memory()
    memory_baseline = mem_info.percent
    # exclude warming up
    model.predict(image_files[0], use_cuda=use_cuda, verbose=False)

    for idx in range(max_num_loops):
        for img in image_files:
            model.predict(img, use_cuda=use_cuda, verbose=False)

        if idx % 100 == 0:
            mem_info = psutil.virtual_memory()
            mem_history.append((datetime.datetime.now(), mem_info.percent - memory_baseline))

    df_mem = pd.DataFrame(mem_history, columns=['time_stamp', 'inc_mem_percent'])
    df_mem.to_csv('cpu_memory_util_history.csv', index=None)

    df_time = pd.DataFrame(model.time_history, columns=['preprocess_time', 'inference_time'])
    df_time.to_csv('inference_time_history.csv', index=None)

    print("---------------")
    print("--End from process: {}   --".format(i))
    print("---------------")


def test(use_cuda=True):
    image_files = ['images/cat.jpg', 'images/cheese.jpg']
    model = Predictor()

    for img in image_files:
        print(model.predict(img, use_cuda))


Model = Predictor()
def predict_work(use_cuda, filePath):

    print("Start to predict!")
    result =  Model.predict(filePath, use_cuda=use_cuda, verbose=False)
    print("End fo predict!")
    return result


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='set input arguments')
    parser.add_argument('--mode', action="store",
                        dest='mode', type=str, default='queue')

    args = parser.parse_args()

    use_cuda_flag = True
    image_files = ['images/cat.jpg', 'images/cheese.jpg']

    if args.mode == 'normal':
        print("Normal predict test")
        test()

    if args.mode == 'smoking':
        print('Smoking predict test')
        smoking_test(max_num_loops=100)

    if args.mode == 'multi':
        print('Multi model predict test')

        pool = multiprocessing.Pool(processes=4)
        smoking_test(use_cuda_flag, 100000)

        for i in xrange(4):
            pool.apply_async(smoking_test, (i, use_cuda_flag, 1000,))

        pool.close()
        pool.join()
        print("test done!")

    if args.mode == 'queue':
        print("Push the predict task to Redis Queue")

        # warm up
        res = predict_work(use_cuda_flag, image_files[0])
        print(res)
        sleep(2)

        jobs = []
        for i in range(10):
            job = q.enqueue_call(func=predict_work, args=(use_cuda_flag, image_files[0],), timeout=5)
            # job = q.enqueue_call(func=rq_test, args=(10, 20,), timeout=5)
            jobs.append(job)
            print(job.id)

        i = 0
        while True:
            job = jobs[i]
            result = Job.fetch(job.id, connection=redis_conn)
            print(result)
            if result.is_finished:
                print(result.return_value)
                del jobs[i]
            else:
                print("job(%d): %d is still working", (i, job.id))
                i  = (i + 1)%10

            if len(jobs) == 0:
                print("ALL work finished!!!")
                break



