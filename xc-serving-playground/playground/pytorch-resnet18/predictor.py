import os
import sys
import torch
import torchvision.models as models
import torchvision.transforms as transforms
from PIL import Image
import psutil
import datetime
import pandas as pd

class Predictor:
    model_names = sorted(name for name in models.__dict__
                         if name.islower() and not name.startswith("__")
                         and callable(models.__dict__[name]))

    def __init__(self, arch='resnet18', checkpoint='model_best.pth.tar'):
        device = torch.device("cuda")
        self.model = models.__dict__[arch]()

        # self.model = torch.nn.DataParallel(models.__dict__[arch](), device_ids=[0])
        #self.checkpoint = torch.load(os.path.join(os.path.dirname(__file__), checkpoint))
        self.checkpoint = torch.load(os.path.join(os.path.dirname(__file__), checkpoint), map_location="cuda:0")

        self.model.load_state_dict(self.checkpoint['state_dict'])
        self.labels = self.checkpoint['labels']
        self.model.to(device)
        self.model.eval()

        self.time_history = []

    def predict(self, input_image, verbose=True):
        img_pil = Image.open(input_image)

        t1 = datetime.datetime.now()

        normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        processor = transforms.Compose([transforms.Scale(256), transforms.CenterCrop(224),
                                        transforms.ToTensor(), normalize, ])
        img_tensor = processor(img_pil)
        img_tensor.unsqueeze_(0)

        img_variable = torch.autograd.Variable(img_tensor)
        t2 = datetime.datetime.now()

        output = self.model(img_variable)
        t3 = datetime.datetime.now()
        # (pp_time, inference_time)
        self.time_history.append(((t2 - t1).total_seconds(), (t3 - t2).total_seconds()))

        result_index = output.data.numpy().argmax()
        result = self.labels[result_index]
        print("Input file path: {}; Prediction Result {}".format(input_image, result))
        return result



def smoking_test(max_num_loops=10000):
    ''' measure 1) host CPU memory util % 2) preprocess and inference time

    :param max_num_loops:
    :return:
    '''

    mem_history = []
    image_files = ['images/cat.jpg', 'images/cheese.jpg']

    model = Predictor()
    mem_info = psutil.virtual_memory()
    memory_baseline = mem_info.percent
    # exclude warming up
    model.predict(image_files[0], verbose=False)

    for idx in range(max_num_loops):
        for img in image_files:
            model.predict(img, verbose=False)

        if idx % 100 == 0:
            mem_info = psutil.virtual_memory()
            mem_history.append((datetime.datetime.now(), mem_info.percent - memory_baseline))

    df_mem = pd.DataFrame(mem_history, columns=['time_stamp', 'inc_mem_percent'])
    df_mem.to_csv('cpu_memory_util_history.csv', index=None)

    df_time = pd.DataFrame(model.time_history, columns=['preprocess_time', 'inference_time'])
    df_time.to_csv('inference_time_history.csv', index=None)

def test():
    image_files = ['images/cat.jpg', 'images/cheese.jpg']
    model = Predictor()

    for img in image_files:
        model.predict(img)

if __name__ == '__main__':
    #test()
    smoking_test(200)

