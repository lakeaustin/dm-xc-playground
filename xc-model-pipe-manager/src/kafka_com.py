# -*- coding:utf-8 -*-
import sys
import six
import json
from kafka import KafkaConsumer, KafkaProducer
from kafka.errors import KafkaError
import logging

logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)

class KafkaWorker(object):

    def __init__(self, woker_type, servers, topic, group_id=None, mode='earliest',key=None, logger=None):
        ''' Build a Kafka producer instance.

        :param woker_type: str; 'producer' or 'consumer'
        :param servers: list of str; a list of Kafka servers.
        :param topic: str; Kafka topic.
        :param key: str;
        '''

        self.bootstrap_servers = servers
        self.topic = topic
        self.key = key
        if logger is not None:
            self.logger = logger
        else:
            self.logger = logging.getLogger("kafka")

        if woker_type == 'producer':
            self.groupid = None
            self.mode = None
            self.logger.info("producer Init:{} {} {}".format(servers, topic, key))

            self.producer = KafkaProducer(bootstrap_servers=servers,
                                          retries=0,
                                          max_block_ms=5000,
                                          request_timeout_ms=2000)
        elif woker_type == 'consumer':

            self.groupid = group_id
            self.mode = mode
            self.logger.info('Consumer Init: {} {} {} {} {}'.format(servers, topic, group_id, mode, key))
            self.consumer = KafkaConsumer(self.topic, group_id=self.groupid,
                                          bootstrap_servers=servers)
        else:
            self.logger.error("Wrong work type, please use 'producer' or 'consumer'!")
            raise ValueError("TBD")


    def send_message(self, payload):
        ''' Convert payload to JSON format and send to Kafka topic
        :payload:
        '''

        # parmas_message = json.dumps(payload, ensure_ascii=False)
        # msg = parmas_message.encode('utf-8')
        msg = payload

        future = self.producer.send(self.topic, msg).add_errback(KafkaWorker.on_send_error)
        self.producer.flush()
        # self.producer.poll(0)
        try:
            record_metadata = future.get(timeout=10)
        except KafkaError as e:
            self.logger.error(e)
            raise RuntimeWarning('TBA')

        self.logger.debug(record_metadata)

    @staticmethod
    def on_send_error(excp):
        sys.stderr.write('%% Message failed delivery\n')
        # if err:
        #     sys.stderr.write('%% Message failed delivery: %s\n' % err)
        # else:
        #     sys.stderr.write('%% Message delivered to %s [%d] @ %o\n' %
        #                      (msg.topic(), msg.partition(), msg.offset()))

    def receive_message(self):
        '''
        
        :return: 
        '''

        """
        http://dm-ai.cn:8090/pages/viewpage.action?pageId=14812991

        [{uuid:"xxxxx",image:"xxxx"} ,……]
        例：
        [
            {
                 uuid:"89931576-54b7-4625-9c0e-518f93d8bdb7",
                 image:"/9j/4AAQSkZJRgAB……"
             }
        ]
        """
        try:
            for msg in self.consumer:
                if msg.topic == self.topic:
                    payload = json.loads(msg.value.decode("utf-8"))
                    self.logger.debug(payload)
                    yield payload
        except Exception as e:
            self.logger.error(e)
            raise RuntimeWarning('TBA')

    def poll(self, timeout=5):
        ''' Polling and receive messages from the Kafka topic

        :param timeout: (int, optional): maximum amount of time to wait (in ms)
                for at least one response.

        :return: a list of payload
        '''
        # get messages as a dict object
        messages = self.consumer.poll(timeout_ms=timeout)

        data = []
        if len(messages) > 0:
            for partition, msgs in six.iteritems(messages):
                if partition.topic == self.topic:
                    for msg in msgs:
                        payload = json.loads(msg.value.decode("utf-8"))
                        data.append(payload)

        self.logger.debug(data)

        return data


if __name__ == '__main__':
    ''' 
    test consumer,producer 
    '''
    import argparse
    from time import sleep
    parser = argparse.ArgumentParser(description='consume/produce')
    parser.add_argument('--mode', action="store",
                        dest='mode', type=str, default='consumer')

    args = parser.parse_args()

    servers = ['127.0.0.1:9092']
    KAFAKA_TOPIC = 'model-input'
    groupid = 'my-group'
    test_type = args.mode
    print(test_type, servers, KAFAKA_TOPIC, groupid)

    msg = b'This is test message'

    if test_type == 'consumer':
        print("Start testing for consume!")
        k = KafkaWorker(test_type, servers, KAFAKA_TOPIC, groupid)

        datas = k.poll(5)
        print('get data from poll:', len(datas))

        datas = k.receive_message()
        for d in datas:
            print('get data from receive_message:', len(d))

    if test_type == 'producer':
        print("Start testing for producer!")
        k = KafkaWorker(test_type, servers, KAFAKA_TOPIC)
        for i in range(60):
            k.send_message(msg)
            print('send_message:', msg)
            sleep(1)


