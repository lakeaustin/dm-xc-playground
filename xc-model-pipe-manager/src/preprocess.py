# -*- coding:utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import logging
import numpy as np
from skimage.transform import resize
from PIL import Image
import pytorch

def pytorch_preprocess(img, target_size=(224, 224), mean=None, std=None):
    ''' use pytorch backend to resize and then normalize the input image (numpy ndarray).

    example for ResNet:
        pytorch_preprocess(img, (224,224), [0.485, 0.456, 0.406], [0.229, 0.224, 0.225])

    :param img: numpy ndarray, in (x, y, c) shape and RGB encoded.
    :param target_size: target size this function will resize image to
    :return: a numpy ndarray, 3D, with the target_size
    '''
    assert (isinstance(img, np.ndarray) and len(img.shape) == 3)

    normalize = pytorch.transforms.Normalize(mean=mean, std=std)

    transform_pipeline = pytorch.transforms.Compose([
        pytorch.transforms.Resize(target_size),
        pytorch.transforms.ToTensor(),
        normalize,
    ])

    pil_img = Image.fromarray(img)
    output_img = transform_pipeline(pil_img)

    return output_img.numpy()



def util_resize(img, target_size=(244, 244), mode='reflect'):
    ''' Perform central_crop and then resize the input image

    :param img: numpy ndarray, in (w,must be 3D array, RGB encoded
    :param target_size: target size this function will resize image to
    :return: a numpy ndarray, 3D, with the target_size
    '''

    assert (isinstance(img, np.ndarray) and len(img.shape) == 3)

    try:
        resized_img = resize(img, target_size, mode=mode)
    except Exception as e:
        print('util_central_crop_and_resize: resize failed. details: {}'.format(e))
        return None

    return resized_img


def util_central_crop_and_resize(img, target_size=(244, 244), mode='reflect'):
    ''' Perform central crop and then resize the input image

    :param img: numpy ndarray, must be 3D array, RGB encoded
    :param target_size: target size this function will resize image to
    :return: a numpy ndarray, 3D, with the target_size
    '''

    assert (isinstance(img, np.ndarray) and len(img.shape) == 3)

    # crop image from center
    short_edge = min(img.shape[:2])
    yy = int((img.shape[0] - short_edge) / 2)
    xx = int((img.shape[1] - short_edge) / 2)
    crop_img = img[yy: yy + short_edge, xx: xx + short_edge]

    try:
        resized_img = resize(crop_img, target_size, mode=mode)
    except Exception as e:
        print('util_central_crop_and_resize: resize failed. details: {}'.format(e))
        return None

    return resized_img

